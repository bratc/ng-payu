import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgPayuModule } from 'projects/ng-payu/src/public-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgPayuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
