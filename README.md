# NgPayu

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.14.

## Install

Run npm install ng-payu

## Use

<ng-payu (formSaved)="processPayment($event)" [planValue]="planValue" [planCode]="0093"></ng-payu>

# Attibutes
   - planCode is number code plan for payment
   - planValue is value to payment 

In your controller .component.ts created on method processPayment(value) {} for send data to server API.
 
## In Development

This project status is development

## Based on

https://www.npmjs.com/package/ng-payment-card

