import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgPayuComponent } from './ng-payu.component';

describe('NgPayuComponent', () => {
  let component: NgPayuComponent;
  let fixture: ComponentFixture<NgPayuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgPayuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgPayuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
