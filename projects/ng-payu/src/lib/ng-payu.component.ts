import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CardValidator } from './validators/card-validator';
import { ICard, Card } from './models/card.model';
import { NgPayuService } from './ng-payu.service';
@Component({
  selector: 'ng-payu',
  templateUrl: './ng-payu.component.html',
  styleUrls: ['./ng-payu.component.css']
})
export class NgPayuComponent implements OnInit {
  formPaymentCard: FormGroup;
  cardDetails: ICard;
  cardType;
  typePayment = true;
  /**
   * List of months
   */
  public months: Array<string> = [];

  /**
   * List of years
   */
  public years: Array<number> = [];
  /**
   * EventEmitter for payment card object
   */
  @Output()
  public formSaved = new EventEmitter<Card>();

  @Input()
  public planValue: number;

  @Input()
  public planCode: string;

  isValid = true;
  constructor(private formBuilder: FormBuilder, private ngPayuService: NgPayuService) {  }

  ngOnInit() {
    this.buildForm();
    this.assignDateValues();
  }

  /**
   * Callback function that emits payment card details after user clicks submit, or press enter
   */
  public emitSavedCard(): void {
    this.cardDetails =  this.formPaymentCard.value;
    this.cardDetails.paymentMethod = this.cardType;
    this.cardDetails.planValue = this.planValue;
    this.cardDetails.planCode = this.planCode;
    this.formSaved.emit(this.cardDetails);
  }

  /**
   * Returns payment card type based on payment card number
   */
  public getCardType(ccNum: string) {
      this.ngPayuService.getCardType(ccNum).then(res => {
      this.cardType = res;
   });
  }

  public onChangeCheckbox() {
    if (this.typePayment === true) {
      this.typePayment = false;
    } else {
      this.typePayment = true;
      this.formPaymentCard.patchValue({
        planIntervalCount: 1
      });
    }
  }
  /**
   * Build Form card
   */

  private buildForm() {
    this.formPaymentCard = this.formBuilder.group({
      cardNumber: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(12),
          Validators.maxLength(19),
          CardValidator.numbersOnly,
          CardValidator.checksum,
        ]),
      ],
      cardHolder: ['', Validators.compose([Validators.required, Validators.maxLength(22)])],
      expirationMonth: ['', Validators.required],
      expirationYear: ['', Validators.required],
      installmentsNumber: [1, Validators.required],
      planIntervalCount: [1, Validators.required],
      documentNumberPayer: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(12),
          CardValidator.numbersOnly,
        ]),
      ],
      ccv: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(4),
          CardValidator.numbersOnly,
        ]),
      ]
    },
    {
      validator: CardValidator.expiration,
    }
    );
  }

  /**
   * Populate months and years
   */
  private assignDateValues(): void {
    this.months = NgPayuService.getMonths();
    this.years = NgPayuService.getYears();
  }

}
