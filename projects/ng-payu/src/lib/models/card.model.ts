export interface ICard {
    cardNumber: string;
    cardHolder: string;
    expirationMonth: string;
    expirationYear: string;
    ccv: number;
    installmentsNumber: number;
    documentNumberPayer: string;
    paymentMethod: string;
    planIntervalCount?: number;
    planValue: number;
    planCode: string;

}

export class Card implements ICard {
    cardNumber: string;
    cardHolder: string;
    expirationMonth: string;
    expirationYear: string;
    ccv: number;
    installmentsNumber: number;
    documentNumberPayer: string;
    paymentMethod: string;
    planIntervalCount?: number;
    planValue: number;
    planCode: string;
}