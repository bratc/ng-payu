import { NgModule } from '@angular/core';
import { NgPayuComponent } from './ng-payu.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [NgPayuComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [NgPayuComponent]
})
export class NgPayuModule { }
