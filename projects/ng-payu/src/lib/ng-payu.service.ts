import { Injectable } from '@angular/core';
import { CardType } from './models/card-type.enum';
import { Month } from './models/month.enum';

@Injectable({
  providedIn: 'root'
})
export class NgPayuService {

  
  /**
   * Return card type based on card number
   */

  cardType: CardType;

  /**
     * Return months in numerical format
     */
  public static getMonths(): Array<Month> {

    const months: Array<Month> = [];
    for (const key of Object.keys(Month)) {
      months.push(Month[key]);
    }
    return months;
  }
  /**
   * Return years based on current year
   */
  public static getYears(): Array<number> {
    const years: Array<number> = [];
    const year = new Date().getFullYear();
    for (let i = -2; i < 5; i++) {
      years.push(year + i);
    }
    return years;
  }

  public getCardType(cardNum) {
    const nameCardType = new Promise((resolve, reject) => {

    if (!this.luhnCheck(cardNum)) {
      return '';
    }
    let payCardType = '';
    const regexMap = [
      { regEx: /^4\d{3}(| |-)(?:\d{4}\1){2}\d{4}$/ig, cardType: CardType.VISA },
      { regEx: /^5[1-5]\d{2}(| |-)(?:\d{4}\1){2}\d{4}$/ig, cardType: CardType.MASTERCARD },
      { regEx: /^3[47][0-9]{3}/ig, cardType: CardType.AMEX },
      { regEx: /^(5[06-8]\d{4}|6\d{5})/ig, cardType: CardType.MAESTRO },
      { regEx: /^(4026|417500|4508|4844|491([37]))/ig, cardType: CardType.VISA_ELECTRON },
      { regEx: /^3[47]\d{1,2}(| |-)\d{6}\1\d{6}$/ig, cardType: CardType.AMERICAN_EXPRESS },
      { regEx: /^36/ig, cardType: CardType.DINERS },
      { regEx: /^6(?:011|5\d\d)(| |-)(?:\d{4}\1){2}\d{4}$/ig, cardType: CardType.DISCOVER }

    ];
    for (let j = 0; j < regexMap.length; j++) {
      if (cardNum.match(regexMap[j].regEx)) {
        payCardType = regexMap[j].cardType;
        break;
      }
    }

    if (cardNum.indexOf('50') === 0 || cardNum.indexOf('60') === 0 || cardNum.indexOf('65') === 0) {
      const g = '508500-508999|606985-607984|608001-608500|652150-653149';
      let i = g.split('|');
      for (let d = 0; d < i.length; d++) {
        const c = parseInt(i[d].split('-')[0], 10);
        const f = parseInt(i[d].split('-')[1], 10);
        if ((cardNum.substr(0, 6) >= c && cardNum.substr(0, 6) <= f) && cardNum.length >= 6) {
          payCardType = 'RUPAY';
          break;
        }
      }
    }
    resolve(payCardType);
    });

    return nameCardType;
  }

  private luhnCheck(cardNum) {
    // Luhn Check Code from https://gist.github.com/4075533
    // accept only digits, dashes or spaces
    const numericDashRegex = /^[\d\-\s]+$/;
    if (!numericDashRegex.test(cardNum)) return false;

    // The Luhn Algorithm. It's so pretty.
    let nCheck = 0, nDigit = 0, bEven = false;
    const strippedField = cardNum.replace(/\D/g, '');

    for (let n = strippedField.length - 1; n >= 0; n--) {
      const cDigit = strippedField.charAt(n);
      nDigit = parseInt(cDigit, 10);
      if (bEven) {
        if ((nDigit *= 2) > 9) nDigit -= 9;
      }

      nCheck += nDigit;
      bEven = !bEven;
    }

    return (nCheck % 10) === 0;
  }
}
